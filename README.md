# **How to test the API** #
**Step 1:**
Please go to the Postman tool using the link below:
[Postman Tool](https://chrome.google.com/webstore/detail/postman-rest-client/fdmmgilgnpjigdojojpjoooidkmcomcm?hl=en)

**Step 2:**
Click on the collections section and then click on import collections as shown in the image below:

![Screen Shot 2015-03-27 at 21.42.34.png](https://bitbucket.org/repo/ojgdaB/images/484176755-Screen%20Shot%202015-03-27%20at%2021.42.34.png)

**Step 3:**
Once you click on import collection a window will pop up please enter the link provided below within the **"Import URL"** Section and click Import

![Screen Shot 2015-03-27 at 21.42.42.png](https://bitbucket.org/repo/ojgdaB/images/2758382695-Screen%20Shot%202015-03-27%20at%2021.42.42.png)
## 
## **LINK TO BE USED FOR IMPORTING:**
**User Routes:** https://www.getpostman.com/collections/03b24b47717b53622cd7

**Reviews Routes:** https://www.getpostman.com/collections/4fd2a8081da5b4b407ca

**Provider Routes:** https://www.getpostman.com/collections/e6bdec0fa6a1a82c394a

**Messages Routes:** https://www.getpostman.com/collections/baf14451748c6a7c1f7c

**Step 4:** 
Once imported you will see collections on the left hand side of the screen please click on the collection to run the test.



# **API Documentation** #




## Find auto-generated API documentation from the code at the following link: http://teamreachapi.herokuapp.com/apiDocs/ ##




## Below is a visual representation of the API with an overview of some of the open-source technologies used to create it: ##
## These resources have all been used because they meet our needs for the API.  ##


![API Image.png](https://bitbucket.org/repo/ojgdaB/images/3011389176-API%20Image.png)
## **[FULL SIZE DOWNLOAD](https://docs.google.com/file/d/0B8sMuODpCN3pR3ZfS1BkRXc1STA/edit?pli=1)
** ##
## Care has been taken to ensure the API is secure: ##
### * All passwords are encrypted before being saved into the database. ##
### * The use of Mongoose validation ensures that no illegal values are posted to the database. ###
### * Passport is used to take care of authentication  ###

=========================================================================================================================================================================================================================================================================================================================================================================================================================================================

To install in cloud9 

>> npm install

To run either

press 'run' when server.js file is selected

or 

>> node server.js
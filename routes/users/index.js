var passport = require("passport");
var User = require('../../models').userModel;

// Export all queries to be available to express app
module.exports = function(app) {

  // Gets all users from database
  app.get('/users', function(req, res) {
    //get a list of all the users
    User.find(function(err, users) {
      if (err)
        return res.send(err);
      return res.status(200).json(users);
    });
  });

  // Gets a single user by ID
  app.get('/users/:user_id', function(req, res) {
    //get a list of all the users
    User.findById(req.params.user_id, function(err, user) {
      if (err){
        return res.send(err);
      }
      if (user){
         return res.status(200).json(user);
      } else {
        return res.status(400).json({message: 'User does not exist with id: ' + req.params.user_id});
      }
    });
  });

  // Adds a user to the database
  app.post('/users',
    function(req, res) {
      var newUser = new User();
      // set the user's  credentials
      newUser.email = req.body.email;
      newUser.password = newUser.generateHash(req.body.password);
      newUser.name = req.body.name;
      newUser.phone = req.body.phone || "";
      newUser.username = req.body.username;

      newUser.save(function(err) {
        if (err) {
          return res.status(400).send(err);
        }
        return res.status(200).send(newUser);
      });
    });

  // Update a single user on there ID
  app.put('/users/:user_id', function(req, res) {
    //Update a users information
    console.log('Update a users information');
    User.findById(req.params.user_id, function(err, doc) {
      if (err)
        return res.status(400).send(err);
      // If no parameter is sent the document element will not be changed.
      // Validation will be run as .save() initates mongoose validation.
      doc.name = req.body.name || doc.name;
      doc.email = req.body.email || doc.email;
      doc.phone = req.body.phone || doc.phone;
      doc.username = req.body.username || doc.name;

      doc.save(function(err) {
        if (err)
          return res.status(400).send(err);
        return res.status(200).json({
          message: "Successfully udpdated users information"
        });
      });
    });
  });
  
  app.delete('/users/:user_id', function(req, res) {
    //Delete a specified user
    User.remove({
      _id: req.params.user_id
    }, function(err, user) {
      if (err)
        return res.status(400).send(err);
      return res.status(200).json({
        message: 'Successfully deleted'
      });
    });
  });
  // Require the next nested route.
  require('./reviews')(app);
};
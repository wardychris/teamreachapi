
var passport = require("passport");
var User = require('../models/').userModel;
var Provider = require('../models/').providerModel;
var Messages = require('../models/').messagesModel;
var Reviews = require('../models/').reviewsModel;

module.exports = function(app) {
  
    app.get('/', function(req, res) {
        res.json({message: "hello and welcome to teamreach api"});
        //res.redirect("http://teamreachdocs.herokuapp.com/apiDocs/");
    });
    
    app.get('/apiDocs/', function(req, res){
        res.redirect("http://teamreachdocs.herokuapp.com/apiDocs/");
    });
  
    
    require('./users')(app);
    require('./providers')(app);
    require('./messages')(app);
    require('./reviews')(app);
    
    // other routes entered here as require(route)(app);
    // we basically pass 'app' around to each route

};

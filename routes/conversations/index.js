'use strict';

// Conversations Route Index
var passport = require("passport");

module.exports = function(app) {
    // Get a conversation
    app.get('/messages/:conversation_id', function(req, res) {
       console.log('GET request on /messages/');
    });
    
    // Post a conversation
    app.post('/messages/:conversation_id', function(req, res) {
        console.log('POST request on /messages/');
    });
    
    // Delete a conversation
    app.delete('/messages/:conversation_id', function(req, res) {
        console.log('DELETE request on /messages/');
    });
    
    // Get a message from a conversation
    app.get('/messages/:conversation_id/:message_id', function(req, res) {
        console.log('GET request on /messages/:conversation_id/');
    });
    
    // Delete a message from a conversation
    app.delete('/messages/:conversation_id/:message_id', function(req, res) {
       console.log('DELETE request on /messages/:conversation_id/'); 
    });
};
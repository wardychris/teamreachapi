///////////////////////////////////////////////////////////////////////////////
//      
//      Routes/messages/index.js
//      -----------
//
//      This is the routes file for messages. These are instant messages
//      between users and providers within an iOS or Android app.
//      
//
///////////////////////////////////////////////////////////////////////////////


'use strict';
var passport = require("passport");
var Message = require("../../models").messagesModel;

////////////////////////////////////////////


// Define functionality for messages route
module.exports = function(app) {
    
    // Get all messages
    app.get('/messages', function(req, res) {
        Message.find(function(err, messages) {
            if(err) return res.send(err);
            return res.status(200).json(messages);
        });
    });
    
    // Get messages from a conversation using Conversation ID
    app.get('/messages/:conversation_id', function(req, res) {
       Message.findById(req.params.conversation_id, function(err, message) {
           if(err) return res.send(err);
           if(message) return res.status(200).json(message);
           else return res.status(400).json({message: 'There are no conversations with the following ID: ' + req.params.conversation_id});
       });
    });
    
    // Post a new message
    app.post('/messages', function(req, res) {
        var newMessage = new Message();
        newMessage.toID = req.body.toID;
        newMessage.fromID = req.body.fromID;
        newMessage.date = Date();
        newMessage.message = req.body.message;
        
        newMessage.save(function(err) {
            if(err) return res.status(400).send(err);
            return res.status(200).send(newMessage);
        });
    });
    
    // Delete a message
    app.delete('/messages/:conversation_id', function(req, res) {
        Message.remove({_id: req.params.conversation_id}, function(err, message) {
            if(err) return res.status(400).send(err);
            return res.status(200).json({
                message: 'Message has been successfully deleted.'
            });
        });
    });
}
// routes/reviews/index.js
// Stores the routes of every URI with /reviews at the begining

var Review = require('../../models').reviewModel;
module.exports = function(app) {
  
   // Post a review to the database
  app.post('/reviews', function(req, res) {
    var newReview = new Review();
    newReview.userID = req.body.userID;
    newReview.providerID = req.body.providerID;
    newReview.rating = req.body.rating;
    newReview.reviewText = req.body.reviewText;
    
    newReview.save(function(err){
      if (err){
        return res.status(400).send(err);
      }
      return res.status(200).json({id: newReview.id, message: 'New review succesfully created'});
    });
  });
  
  
  // Get reviews for a certain provider
  app.get('/reviews/provider/:provider_id/', function(req, res) {
    
     Review.find().where('providerID', req.params.provider_id).exec(function(err, doc) {
      if (err) {
        return res.status(400).send(err);
      }
      if (doc.length == 0) {
        return res.status(400).json({message: 'no documents found for provider with id: ' + req.params.provider_id});
      } else {
        return res.status(200).send(doc);
      }
      });
  });
  
  // Get reviews for a certain user
  app.get('/reviews/user/:user_id/', function(req, res) {
    
     Review.find().where('userID', req.params.user_id).exec(function(err, doc) {
      if (err) {
        return res.status(400).send(err);
      }
      if (doc.length == 0) {
        return res.status(400).json({message: 'no documents found for user with id: ' + req.params.provider_id});
      } else {
        return res.status(200).send(doc);
      }
      });
  });
  
  // Update a review
  app.put('/reviews/:id', function(req, res) {
    //Update a review's rating
    console.log('Update a reviews rating');
    
    Review.findById(req.params.id, function(err, doc) {
        if (err) throw err;
        // If no parameter is sent the document element will not be changed.
        // Validation will be run as .save() initates mongoose validation.
        doc.rating = req.body.rating || doc.rating;
        doc.reviewText = req.body.reviewText || doc.reviewText;
        
        doc.save(function(err){
          if (err) throw err;
          res.json({
            message: "Successfully updated the review",
            review: doc
          });
        });
      });
  });
  
  //Delete a review
  app.delete('/reviews/:id', function(req, res) {
    
    Review.remove({
      _id: req.params.id
      }, function(err, user) {
        if (err) {
         return res.status(400).send(err);
        }
        return res.status(200).json({
          message: 'Successfully deleted'
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////
//      
//      Providers.js
//      -----------
//
//      This is the Mongoose model for providers. This were all the 
//      the routes for providers exist.
//      
//
///////////////////////////////////////////////////////////////////////////////


var passport = require("passport");
var Provider = require('../../models').providerModel;

module.exports = function(app) {
  
  /////////////////////////////////////////////  
  // GETTING ALL PROVIDERS
  /////////////////////////////////////////////
  app.get('/providers', function(req, res) {
    // Gets all of providers in the database
    Provider.find(function(err, providers) {
      if (err) {
        return res.status(400).send(err);

      }
      return res.status(200).send(providers);
    });
    console.log('Gets all of providers in the database');
  });

  app.post('/providers/find', function(req, res) {
    // Gets all the providers based on the search text entered by the user.
    Provider.find({'providerInfo.keyWords': 'req.body.search'}, 
    function(error, models) {
      if (error) {
        return res.status(400).send(error);

      }
      return res.status(200).send(models);
    });
      //put code to process the results here
  });
  
  
   /////////////////////////////////////////////  
  // UPDATING A PROVIDERS KEYWORDS ARRAY
  /////////////////////////////////////////////
  app.put('/provider/words/:id', function(req, res) {

    Provider.findById(req.params.id, function(err, doc) {
      if (err)
        return res.send(err);

      if (req.body.keyWords) {
        var keywordArray = req.body.keyWords.split(',');
        for (var x in keywordArray) {
          doc.providerInfo.keyWords.push(keywordArray[x]);
        }
      }

      doc.save(function(err) {
        if (err)
          return res.send(err);
        return res.json({
          message: "Successfully udpdated users information"
        });
      });
    });
  });



  /////////////////////////////////
  // CREATING NEW PROVIDERS
  ////////////////////////////////
  app.post('/providers',
    function(req, res) {

      var newProvider = new Provider();

      newProvider.email = req.body.email;
      newProvider.password = newProvider.generateHash(req.body.password);
      newProvider.providerInfo.firstName = req.body.firstName;
      newProvider.providerInfo.lastName = req.body.lastName;
      newProvider.providerInfo.businessBio = req.body.businessBio;
      newProvider.providerInfo.businessName = req.body.businessName;
      newProvider.providerInfo.contactDetails.phone = req.body.phone;
      if (req.body.keyWords) {
        var keywordArray = req.body.keyWords.split(',');
        for (var x in keywordArray) {
          newProvider.providerInfo.keyWords.push(keywordArray[x]);
        }
      }
      return newProvider.save(function(err) {
        if (!err) {
          console.log("User Created");
        }
        else {
          return res.status(400).send(err);
        }
        return res.status(200).send(newProvider);
      });

    });

  /////////////////////////////////
  // UPDATING A SINGLE PROIVDER
  /////////////////////////////////
  app.put('/providers/:provider_id', function(req, res) {
    // Update information
    // Using mongoose in-built function "findbyID"
    Provider.findById(req.params.provider_id,
      function(err, provider) {
        provider.providerInfo.firstName = req.body.firstName || provider.providerInfo.firstName;
        provider.providerInfo.lastName = req.body.lastName || provider.providerInfo.lastName;
        provider.providerInfo.businessBio = req.body.businessBio || provider.providerInfo.businessBio;
        provider.providerInfo.businessName = req.body.businessName || provider.providerInfo.businessName;
        provider.providerInfo.contactDetails.phone = req.body.phone || provider.providerInfo.contactDetails.phone;
        provider.providerInfo.keyWords = req.body.keyWords || provider.providerInfo.keyWords;

        return provider.save(function(err) {
          if (!err) {
            console.log("updated");
          }
          else {
            return res.status(400).send(err);
          }
          return res.status(200).send(provider);
        });
      });

  });

  /////////////////////////////////
  // DELETING A SINGLE PROVIDER
  ////////////////////////////////
  app.delete('/providers/:provider_id/', function(req, res) {
    // Delete all information for this provider

    Provider.findById(req.params.provider_id,
      function(err, provider) {

        Provider.remove({
          _id: req.params.provider_id
        }, function(err, provider) {
          if (err)
            return res.status(400).send(err);

          res.json({
            message: 'Provider was succesfuly removed from the database'
          });
        });

        console.log('Delete all information for this provider');
      });
  });

};
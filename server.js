// var db = require('./mongoose');


//|| ADD SERVER REQUIRES ||//
var mongoose = require("mongoose");
var express = require('express');
var bodyParser = require('body-parser');

// Authentication modules // 
var passport = require('passport');
var flash    = require('connect-flash');
var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var session      = require('express-session');


var port = process.env.PORT || 8080;  
var app = express();
var configDB = require('./config/dbconfig.js');

// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)

// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// set our port
// note that I'm leaving out the other things like 'http' or 'path'

// get the routes
require('./routes')(app, passport);
// I just require routes, without naming it as a var, & that I pass (app)


app.listen(port);
console.log('Magic happens on port ' + port);
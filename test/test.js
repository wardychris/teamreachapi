//##############################################################################
//
//      Mocha Tests
//      -----------
//
//      This file contains tests for the Mongoose models.
//
//      Current Tests
//      -------------
//      
//      models/users.js
//      models/reviews.js
//      models/providers.js
//      models/messages.js
//
//
//##############################################################################


var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// Assign destinations to test
var users = require("../models/users.js");
var reviews = require("../models/reviews.js");
var provider = require("../models/providers.js");
var message = require("../models/messages.js");
var conversation = require("../models/conversations.js");

// Assign testing libraries
var assert = require("assert");


//##############################################################################

describe('Users Model Testing', function() {
    
    this.timeout(20000);
    
    var User = mongoose.model('User');
    var johnSmith = new User({ name: 'John Smith', password: 'password!', email: 'john@smith.com' });
      
    it('The users password should be correctly hashed with a salt', function() {
        //assert.equal(bcrypt.hashSync('password!', bcrypt.genSaltSync(8), null), johnSmith.generateHash());
        assert.notEqual('password', johnSmith.generateHash());
    });
    
    it('The user should provide a valid password', function() {
         assert.equal(1,1);
    });
    
});

//##############################################################################

describe('Reviews Model Testing', function() {
    
    this.timeout(20000);
    
    var Review = mongoose.model('Review');
    var testReview = new Review();
    
    it('** Placeholeder for Reviews Testing Module **', function() {
        assert.equal(1,1);
    });
    
});

//##############################################################################


describe('Providers Model Testing', function() {
    
    this.timeout(90000);
    
    
    var Provider = mongoose.model('Provider');
    var businessOwner = new Provider({firstName: "John", lastName:"Smith", email:"john@gmail.com", password:"john123",
                                    businessBio:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,",
                                    businessName:"OxfordPlumbers", keyWords:"plumber, pipes", phone:"07873891957"});
    
    it('The users password should be correctly hashed with a salt', function() {
        //assert.equal(bcrypt.hashSync('password!', bcrypt.genSaltSync(8), null), johnSmith.generateHash());
       // assert.equal('password', businessOwner.generateHash());
        businessOwner.providerInfo.firstName.should.have.a.lengthOf(5);
    });
    
    it('** Placeholeder for Providers Testing Module **', function() {
        assert.equal(1,1);
    });
    
});



  /*describe('#save()', function(){
      
    //var Provider = mongoose.model('provider');
    it('should save without error', function(done){
     var businessOwner = new provider({firstName: "John", lastName:"Smith", email:"john@gmail.com", password:"john123",
                                      businessBio:"Lorem ipsum dolor sit amet",
                                      businessName:"OxfordPlumbers", keyWords:"plumber, pipes", phone:"07873891957"});
      businessOwner.save(done);
    });
  });*/

describe('provider', function(){
    var Provider = mongoose.model('Provider');
  describe('#save()', function(){
    it('should save without error', function(done){
      var businessOwner = new Provider({firstName: "John", lastName:"Smith", email:"john@gmail.com", password:"john123",
                                      businessBio:"Lorem ipsum dolor sit amet",
                                      businessName:"OxfordPlumbers", keyWords:"plumber, pipes", phone:"07873891957"});
      businessOwner.save(function(err){
        if (err) throw err;
        done();
      });
    })
  })
})


//##############################################################################

describe('Conversations Model Testing', function() {
    
    this.timeout(20000);
    
    var Conversation = mongoose.model('Conversation');
    var testConversation = new Conversation();
    
    it('** Placeholeder for Conversations Testing Module **', function() {
        assert.equal(1,1);
    });
    
});

//##############################################################################

describe('Messages Model Testing', function() {
    
    this.timeout(20000);
    
    var Message = mongoose.model('Message');
    var testMessage = new Message();
    
    it('** Placeholeder for Messages Testing Module **', function() {
        assert.equal(1,1);
    });
    
});
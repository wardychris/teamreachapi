///////////////////////////////////////////////////////////////////////////////
//      
//      Messages.js
//      -----------
//
//      This is the Mongoose model for messages. These are instant messages
//      between users and providers within an iOS or Android app.
//      
//
///////////////////////////////////////////////////////////////////////////////

'use strict';

////////////////////////////////////////////


// Declare Mongoose and validation variables
var mongoose = require('mongoose'),
    validate = require('mongoose-validator'),
    bcrypt = require('bcrypt-nodejs'),
    Schema = mongoose.Schema;

// Declare validation required for new messages
var messageValidator = [
    
    // Validate that a message is not null and less than or equal to 1000 characters
    validate({
        validator: 'isLength',
        arguments: [1, 1000],
        message: 'The message should be less than 1000 words, and not empty'
    })
    
];


////////////////////////////////////////////


// Define the messages Mongoose model
var MessagesSchema = new Schema({
    
    // ID of the message reciever (User/Provider)
    toID: {
        type: String,
        required: true
    },
    
    // ID of the messge sender (User/Provider)
    fromID: {
        type: String,
        required: true
    },
    
    // The date/time of the message
    date: {
        type: Date,
        required: true
    },
    
    // The message content
    message: {
        type: String,
        required: true
            // Add character limit
    },
    
    // Indicates whether the reciever has read the message.
    toSeen: Boolean,
    
    // Indicates that the sender has been notified of delivery
    fromSeen: Boolean
    
});


////////////////////////////////////////////


// Define the Mongoose save action
MessagesSchema.pre('save', function(next) {
    var currentDateTime = new Date();

    this.updated_at = currentDateTime;

    if (!this.created_at) {
        this.created_at = currentDateTime;
    }
});


////////////////////////////////////////////

// Export the messages model
var Message = mongoose.model('Message', MessagesSchema);
module.exports = Message;

// Validate the length of a message
Message.schema.path('message').validate(function(value) {
    return /1000/i.test(value.length);
}, 'Message text is too long. Maximum of 1000 characters allowed.');

// Validate the date given to a message is in the correct format.
Message.schema.path('date').validate(function(value) {
    return /^(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})$/.test(value.length);
}, 'The message date format is not valid');
'use strict';

var mongoose = require('mongoose'),
    validate = require('mongoose-validator'),
    Schema = mongoose.Schema;

// Validation Functions ========#

// # === Rating === #
var ratingValidator = [
  validate({
    validator: 'isLength',
    arguments: [1, 1],
    passIfEmpty: false,
    message: 'Rating should only be of length 1, e.g 1 or 2 or 3 or 4 or 5'
  }),
  validate({
    validator: 'isInt',
    message: 'Rating must be a whole number'
  }),
  validate({
    validator: 'isIn',
    arguments: ["0 1 2 3 4 5"],
    message: 'Rating must be between 0 and 5'
  })
];

// # === Review Text === #
var reviewTextValidator = [
  validate({
    validator: 'isLength',
    arguments: [1, 2500],
    passIfEmpty: false,
    message: 'Review text can\'t be empty but musn\'t exceed 2500 characters'
  }),
];

var userIDValidator =[
  validate({
    validator: 'isAlphanumeric',
    passIfEmpty: false,
    message: 'UserID must be a valid alphanumeric string'
  })
];

var providerIDValidator =[
  validate({
    validator: 'isAlphanumeric',
    passIfEmpty: false,
    message: 'ProviderID must be a valid alphanumeric string'
  })
];

// End of Validation Functions =#

// Schema object
var ReviewSchema = new Schema({
    userID: {
      type: String,
      required: true,
      validate: userIDValidator
    },
    providerID: {
      type: String,
      required: true,
      validate: providerIDValidator
    },
    rating: {
        type: Number,
        required: true,
        validate: ratingValidator
    },
    reviewText: {
        type: String,
        required: true,
        validate: reviewTextValidator
    },
    created_at: Date,
    updated_at: Date
});

// Set created at or updated at values

ReviewSchema.pre('save', function(next){
  var currentDateTime = new Date();
  this.updated_at = currentDateTime;
  
  if (!this.created_at){
    this.created_at = currentDateTime;
  }
  next();
});

var Review = mongoose.model('Review', ReviewSchema);
module.exports = Review;


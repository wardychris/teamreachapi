'use strict';

var mongoose = require('mongoose'),
    validate = require('mongoose-validator'),
    bcrypt = require('bcrypt-nodejs'),
    Schema = mongoose.Schema;

// Validation Functions ========#

// # === Name === #
var nameValidator = [
  validate({
    validator: 'isLength',
    arguments: [2, 30],
    message: 'Name should be between 3 and 30 characters'
  }),
  validate({
    validator: 'isAlpha',
    passIfEmpty: false,
    message: 'Name should contain alphabetical characters only'
  })
];

// # === Username === #
var usernameValidator = [
  validate({
      validator: 'isAlphanumeric',
      passIfEmpty: false,
      message: 'Username must contain only letters and numbers'
  }),
];

var emailValidator = [
  validate({
      validator: 'isEmail',
  })  
];

// # === Phone === #
var phoneValidator = [
    validate({
      validator: 'isNumeric',
      //if local phone number is set example numbers in Great Britain.
      // arguments: ['en-GB']
      message: 'not a phone number'
    })
];


// End of Validation Functions =#

// Create user schema
var UserSchema = new Schema({
    name: {
        type: String,
        required: true,
        validate: nameValidator
    },
    username: {
        type: String,
        required: true,
        unique: true,
        validate: usernameValidator
    },
    password: {
        type: String
    },
    email: {
        type: String,
        required: true,
        unique: true,
        validate: emailValidator
    },
    conversations: [{ConvoID: String, message: String, time: Date}],
    Review: [{ReviewID: String, message: String, time: Date}],
    phone: {
        type: String,
        validate: phoneValidator
    },
     created_at: Date,
     updated_at: Date
});

// Save Methods
UserSchema.pre('save', function(next){
  
  var currentDateTime = new Date();
  
  this.updated_at = currentDateTime;
  
  if (!this.created_at){
    this.created_at = currentDateTime;
  }
  next();
});


// methods =====================#
// generating a hash
UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};
// End of User Methods =========#

// Export User Model to /models/index.js
var User = mongoose.model('User', UserSchema);
module.exports = User;

'use strict';

var mongoose = require('mongoose'),
    bcrypt = require('bcrypt-nodejs'),
    validate = require('mongoose-validator'),
    Schema = mongoose.Schema;

// Validation Functions ========#

// # === Businees Name === #
var businessNameValidator = [
  validate({
    validator: 'isLength',
    arguments: [2, 50],
    message: 'Business Name should be between 3 and 50 characters'
  }),
  validate({
    validator: 'isAlphanumeric',
    passIfEmpty: false,
    message: ' Business Name should contain alphabetical characters and numbers only'
  })
];

// # === Business Bio === #
var businessBioValidator = [
  validate({
    validator: 'isLength',
    arguments: [2, 140],
    message: 'Business Bio should be between 3 and 140 characters'
  })
];

// # === Providers Email === #
var emailValidator = [
  validate({
    validator: 'isEmail',
    passIfEmpty: false,
    message: 'Email entered is not of correct format'

  })
];

// # === Phone === #
var phoneValidator = [
  validate({
    validator: 'isNumeric',
    passIfEmpty: true,
    message: 'Phone number entered is not of correct format'
  })

];

// # === First Name === #
var firstNameValidator = [
  validate({
    validator: 'isAlpha',
    passIfEmpty: false,
    message: 'First Name should contain alphabetical characters only'
  })
];


// # === Last Name === #
var lastNameValidator = [
  validate({
    validator: 'isAlpha',
    passIfEmpty: false,
    message: 'Last Name should contain alphabetical characters only'
  })
];


// End of Validation Functions =#




var ProviderSchema = new Schema({

  email: {
    type: String,
    required: true,
    unique: true,
    validate: emailValidator
  },

  password: {
    type: String,
    required: true
  },

  providerInfo: {

    businessName: {
      type: String,
      required: true,
      unique: true,
      validate: businessNameValidator
    },

    businessBio: {
      type: String,
      //required: true,
      validate: businessBioValidator
    }, // only 140 characters


    firstName: {
      type: String,
      required: true,
      validate: firstNameValidator
    },

    lastName: {
      type: String,
      required: true,
      validate: lastNameValidator
    },

    contactDetails: {

      phone: {
        type: String,
        required: false,
        unique: true,
        validate: phoneValidator
      },
    },
   keyWords: [],
  },
 
  created_at: Date,
  updated_at: Date
});

// Save Methods

// Set created at or updated at values
ProviderSchema.pre('save', function(next) {
  var currentDateTime = new Date();

  this.updated_at = currentDateTime;

  if (!this.created_at) {
    this.created_at = currentDateTime;
  }
  next();
});


// generating a hash
ProviderSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
ProviderSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.local.password);
};

var Provider = mongoose.model('Provider', ProviderSchema);

module.exports = Provider;
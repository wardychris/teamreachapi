'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Create schema
var ConversationSchema = new Schema({
    userID: {
        type: String,
        required: true
    },
    providerID: {
        type: String,
        required: true
    },
    messageID: [{
        messageID: String
    }],
    messageCount: Number,
    lastUpdate: Date
});

var Conversation = mongoose.model('Conversation', ConversationSchema);
module.exports = Conversation;

Conversation.schema.path('lastUpdate').validate(function(value) {
    return /^(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2}):(\d{2})$/.test(value.length);
}, 'Last update date/time format invalid');
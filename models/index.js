// models/index.js

// Export models
// =============================================================================

// models
var User = require('./users'),
    Review = require('./reviews'),
    Provider = require("./providers"),
    Message = require("./messages");

// exports
exports.userModel = User;
exports.providerModel = Provider;
exports.reviewModel = Review;
exports.messagesModel = Message;
